terraform {
  extra_arguments "common_vars_plan" {
    commands = [
      "plan"]

    arguments = [
      "-var-file=integration.tfvars"
    ]
  }

  extra_arguments "common_vars_autoapprouve" {
    commands = [
      "apply","destroy"]

    arguments = [
      "-var-file=integration.tfvars",
      "-auto-approve"
    ]
  }
}
remote_state {
  backend = "s3"

  config = {
    bucket          = "yayi-712326992774-tfstates"
    key             = "322421052233/fondation/${path_relative_to_include()}.tfstate"
    region          = "eu-west-1"
  }
}
