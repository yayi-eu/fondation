variable "aws_region" {
  description = "AWS region"
}

variable "amazonses_verification_records" {
  type = list
  description = "the amazon ses verfication records for all organizations"
}
