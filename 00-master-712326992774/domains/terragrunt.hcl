include {
  path = find_in_parent_folders()
}
dependency "develop_ses"{
  config_path = "../../10-develop-083446495700/ses"
}
dependency "uat_ses"{
  config_path = "../../30-uat-513800838404/ses"
}
dependency "prod_ses"{
  config_path = "../../40-prod-417563820568/ses"
}
inputs = {
  amazonses_verification_records = [dependency.develop_ses.outputs.ses_verification_token,
    dependency.prod_ses.outputs.ses_verification_token,dependency.uat_ses.outputs.ses_verification_token]
}


