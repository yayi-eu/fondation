provider "aws" {
  region     = var.aws_region

}
terraform {
  backend "s3" {
  }
  required_providers {
    aws = {
      source  = "registry.terraform.io/hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
locals {
  domain_talkywalky_net = "talkywalky.net"
}

module "talkywalky_net" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//domain?ref=v1.1.17"
  #source = "../../../tf-modules/domain"
  aws_region =  var.aws_region
  domain = local.domain_talkywalky_net
  is_mx_google = true
  google_verification_record = "google-site-verification=2x9sEG1bRyXEuoBKqL9ff0B84igAgM8T4eC3lpxvMtg"
  amazonses_verification_records = var.amazonses_verification_records
}
