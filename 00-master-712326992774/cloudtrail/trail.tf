locals {
  trail_bucket_name = "yayi-master-trail"
}
resource "aws_cloudtrail" "master" {
  name = "yayitrail"
  s3_bucket_name = local.trail_bucket_name
  include_global_service_events = true
  is_multi_region_trail = false
  is_organization_trail = true
}
resource "aws_s3_bucket" "master_trail" {
  bucket = local.trail_bucket_name
  force_destroy = true
  region = var.aws_region
  versioning {
    enabled = false
  }
  policy = data.aws_iam_policy_document.trail_bucket_policy.json
}
data "aws_iam_policy_document" "trail_bucket_policy"{
  provider = aws
  statement {
    sid = "AWSCloudTrailAclCheck"
    effect = "Allow"
    actions = [
      "s3:GetBucketAcl"
    ]
    resources = [
      "arn:aws:s3:::${local.trail_bucket_name}"
    ]
    principals {
      type = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
  statement {
    sid = "AWSCloudTrailWrite"
    effect = "Allow"
    principals {
      identifiers = [
        "cloudtrail.amazonaws.com"]
      type = "Service"
    }
    actions = [
      "s3:PutObject"]
    resources = [
      "arn:aws:s3:::${local.trail_bucket_name}/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
    ]
  }
    statement {
      sid = "AWSCloudTrailWriteOrg"
      effect = "Allow"
      principals {
        identifiers = ["cloudtrail.amazonaws.com"]
        type = "Service"
      }
      actions = ["s3:PutObject"]
      resources = [
        "arn:aws:s3:::${local.trail_bucket_name}/AWSLogs/o-5duek66rtj/*",
      ]

    }

}
data "aws_caller_identity" "current" {}
