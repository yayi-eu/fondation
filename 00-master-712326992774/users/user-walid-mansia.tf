module "user_walid_mansia" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//aws-user?ref=v1.1.0"
  name                              = "walid.mansia"
  groups                            = [var.grp_devops_name]
  force_destroy                     = true
  pgp_key                           = "keybase:walidportago"
}
output "walid_mansia_user_name" {
  value = module.user_walid_mansia.aws_iam_user-credentials.name
}
output "walid_mansia_access_key_id" {
  value = module.user_walid_mansia.aws_iam_user-credentials.access-key-id
}
#terraform output walid_mansia_encrypted_password | base64 --decode | keybase pgp decrypt
output "walid_mansia_encrypted_secret_access_key" {
  value = module.user_walid_mansia.aws_iam_user-credentials.encrypted-secret-access-key
}
# terraform output walid_mansia_encrypted_password | base64 --decode | keybase pgp decrypt
output "walid_mansia_encrypted_password" {
  value = module.user_walid_mansia.aws_iam_user-credentials.encrypted_password
}

