include {
  path = find_in_parent_folders()
}

dependency "groups"{
  config_path = "../groups"
}
inputs = {
  grp_devops_name = dependency.groups.outputs.grp_devops_name
}

