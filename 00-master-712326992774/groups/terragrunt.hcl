include {
  path = find_in_parent_folders()
}

dependency "roles"{
  config_path = "../roles"
}
inputs = {
  deployer_role_policy_json = dependency.roles.outputs.deployer_role_policy_json
}


