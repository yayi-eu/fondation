resource "aws_organizations_policy" "simple" {
  name = "simple"

  content = <<CONTENT
{
  "Version": "2012-10-17",
  "Statement": [
  {
    "Sid": "AllowAcm",
    "Effect": "Allow",
    "Action": "acm:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowApigateway",
    "Effect": "Allow",
    "Action": "apigateway:*",
    "Resource": "*"
  },
  {
    "Sid": "DenyCloudfront",
    "Effect": "Deny",
    "Action": "cloudfront:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowCloudwatch",
    "Effect": "Allow",
    "Action": "cloudwatch:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowCognito",
    "Effect": "Allow",
    "Action": "cognito:*",
    "Resource": "*"
  },
  {
    "Sid": "DenyEc2",
    "Effect": "Deny",
    "Action": "ec2:*",
    "Resource": "*"
  },
  {
    "Sid": "DenyEcs",
    "Effect": "Deny",
    "Action": "ecs:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowIam",
    "Effect": "Allow",
    "Action": "iam:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowLambda",
    "Effect": "Allow",
    "Action": "lambda:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowLogs",
    "Effect": "Allow",
    "Action": "logs:*",
    "Resource": "*"
  },
  {
    "Sid": "DenyRoute53",
    "Effect": "Deny",
    "Action": "route53:*",
    "Resource": "*"
  },
  {
    "Sid": "AllowS3",
    "Effect": "Allow",
    "Action": "s3:*",
    "Resource": "*"
  },
  {
    "Sid": "DenySts",
    "Effect": "Deny",
    "Action": "sts:*",
    "Resource": "*"
  }
]
}
CONTENT
}
