provider "aws" {
  region     = var.aws_region
}
terraform {
  backend "s3" {
  }
}

module "develop" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//organization?ref=v1.1.0"
  aws_region        = var.aws_region
  ou_name           = "develop"
  master_ou_id      = "r-kon9"
  ou_email          = "walid.mansia+develop@yayi.eu"
  ou_policy_id      = aws_organizations_policy.simple.id
}

module "integration" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//organization?ref=v1.1.0"
  aws_region        = var.aws_region
  ou_name           = "integration"
  master_ou_id      = "r-kon9"
  ou_email          = "walid.mansia+integration@yayi.eu"
  ou_policy_id      = aws_organizations_policy.simple.id
}

module "uat" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//organization?ref=v1.1.0"
  aws_region        = var.aws_region
  ou_name           = "uat"
  master_ou_id      = "r-kon9"
  ou_email          = "walid.mansia+uat@yayi.eu"
  ou_policy_id      = aws_organizations_policy.simple.id
}

module "prod" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//organization?ref=v1.1.0"
  aws_region        = var.aws_region
  ou_name           = "prod"
  master_ou_id      = "r-kon9"
  ou_email          = "walid.mansia+prod@yayi.eu"
  ou_policy_id      = aws_organizations_policy.simple.id
}
