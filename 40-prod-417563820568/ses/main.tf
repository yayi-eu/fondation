provider "aws" {
  version = "2.70.0"
  region     = var.aws_region
  assume_role {
    role_arn = var.provider_env_roles
  }
}

provider "aws" {
  version = "2.70.0"
  region     = var.aws_region
  alias = "route53"
}
terraform {
  backend "s3" {
  }
}
data "aws_route53_zone" "talkywalky" {
  name = var.domain
  private_zone = false
  provider = aws.route53
}
module "talkywalky" {
  source = "git@gitlab.com:yayi-eu/tf-modules.git//roles?ref=v1.1.17"
  #source = "../../../tf-modules/ses"
  aws_region = var.aws_region
  provider_env_roles  = var.provider_env_roles
  domain = var.domain
  zone_id = data.aws_route53_zone.talkywalky.zone_id
  providers = {
    aws =aws
    aws.route53 = aws.route53
  }
}
