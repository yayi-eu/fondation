terraform {
  extra_arguments "common_vars_plan" {
    commands = [
      "plan","validate"]

    arguments = [
      "-var-file=local.tfvars",
      "-no-color"
    ]
  }

  extra_arguments "common_vars_autoapprouve" {
    commands = [
      "apply","destroy"]

    arguments = [
      "-var-file=local.tfvars",
      "-auto-approve",
      "-no-color"
    ]
  }
}
remote_state {
  backend = "s3"

  config = {
    bucket          = "yayi-712326992774-tfstates"
    key             = "docker-local/docker-local/${path_relative_to_include()}.tfstate"
    region          = "eu-west-1"
  }
}
