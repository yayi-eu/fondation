terraform {
  backend "s3" {
  }
  required_providers {
    docker = {
      source = "terraform-providers/docker"
    }
  }
}

provider "docker" {
  host = var.docker_host
}
resource "null_resource" "runner_token" {
  #count = fileexists("token.file") ? 0 : 1
  triggers = {
    always = uuid()
  }
  provisioner "local-exec" {
    command= "curl --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\" \"https://gitlab.com/api/v4/groups/9208018\" | jq -r .runners_token   > ./token.file"
  }
}

module "gitlab_runner" {
  depends_on = [null_resource.runner_token]
#  source = "git@gitlab.com:yayi-eu/tf-modules.git//gitlab_runner?ref=v1.0.0"
  source = "../../../tf-modules/gitlab_runner"
  runner_name                   = var.runner_name
  runner_token                  = fileexists("token.file") ? chomp(file("token.file")) : 0
  runner_volumes                = var.runner_volumes
  gitlab_runner_image           = var.gitlab_runner_image
  runner_base_image             = var.runner_base_image
  runner_m2_path                = var.runner_m2_path
  runner_data_path              = var.runner_data_path
  runner_docker_container_name  = var.runner_docker_container_name
  providers = {
    docker = docker
  }
}

